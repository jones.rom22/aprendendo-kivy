from kivy.app import App
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from kivy.properties import ObjectProperty
import json

class AppScreenManager(ScreenManager):
    pass

class Menu(Screen):
    pass

class ListaTarefas(Screen):
    tarefas = []
    tarefas_panel = ObjectProperty(None)
    input_descricao_tarefa = ObjectProperty(None)
    path = ''

    def __init__(self, tarefas=[], **kwargs):
        super().__init__(**kwargs)
    
    def on_pre_enter(self):
        Window.bind(on_keyboard=self.voltar)
        self.path = App.get_running_app().user_data_dir+'/'
        self.loadData()
        for tarefa in self.tarefas:
            self.tarefas_panel.add_widget(Tarefa(tarefa))
    
    def voltar(self, window, key, *args):
        if key == 27:
            App.get_running_app().root.current = 'menu'
            return True
    
    def on_pre_leave(self):
        Window.unbind(on_keyboard=self.voltar)
    
    def loadData(self, *args):
        try:
            with open(self.path + 'data.json', 'r') as data:
                self.tarefas = json.load( data)
        except:
            pass

    def saveData(self, *args):
        with open(self.path + 'data.json', 'w') as data:
            json.dump(self.tarefas, data)

    def add_tarefa(self):
        tarefa = Tarefa(self.input_descricao_tarefa.text)
        self.tarefas.append(self.input_descricao_tarefa.text)
        self.tarefas_panel.add_widget(tarefa)
        self.input_descricao_tarefa.text = ''
        self.input_descricao_tarefa.focus = True
        print(self.tarefas)
        self.saveData()

    def remove_tarefa(self, widget, description):
        self.tarefas_panel.remove_widget(widget)
        self.tarefas.remove(description)
        print(self.tarefas)
        self.saveData()

class Tarefa(BoxLayout):
    def __init__(self, task_description, **kwargs):
        super().__init__(**kwargs)
        self.ids.desc_tarefa.text = task_description

class TestApp(App):
    def build(self):
        return AppScreenManager()


if __name__ == "__main__":
    TestApp().run()